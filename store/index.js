import Vuex from 'vuex'
import axios from 'axios'

const createStore=()=>{
    return new Vuex.Store({
        state:{
            loadedPosts:[],
            token:null
        },
        mutations:{
            setPosts(state, posts){
                state.loadedPosts=posts;
            },
            addPost(state, post){
                state.loadedPosts.push(post)
            },
            editPost(state, editedPost){
                const postIndex=state.loadedPosts.findIndex(
                    post=> post.id===editedPost.id)
                    state.loadedPosts[postIndex]=editedPost
            },
            setToken(state, token){
                state.token=token
            }
        },
        actions:{
            nuxtServerInit(vuexContext, context){
              return axios.get('http://localhost:3000/allPosts')
              .then(res=>{
                  const postsArray=[]
                  for(const key in res.data){
                      postsArray.push({...res.data[key],id:key})
                  }
                  
                  vuexContext.commit('setPosts',postsArray)
              })
              .catch(e=>context.error(e))
            },
            setPosts(vuexContext, posts){
                vuexContext.commit('setPosts',posts)
            },addPost(vuexContext , postData){
                const createdPost={
                    ...postData,
                    updatedDate: new Date()
                }
                axios.post('http://localhost:3000/createPost',{...postData,updatedDate:new Date()})
                .then(res=>{
                    vuexContext.commit('addPost',{...createdPost,id:res.data.name})
                    
                }
                    ).catch(err=>console.log(err))
          
            },
            editPost(vuexContext, editedPost){
                return axios.post('http://localhost:6500/editPost',editedPost)
                .then(res=>
                vuexContext.commit('editPost',editedPost)
                )
                .catch(e=>console.log(e))
            },
            authenticateUser(vuexContext, authData){
                let loginUrl="http://localhost:6500/auth/login"
                if(!authData.isLogin){
                  loginUrl='http://localhost:6500/auth/register'
                }
                return this.$axios.$post(loginUrl,
                {
                  usename:authData.email,
                  password:authData.password
                })
                .then(res=>{console.log(res);
                vuexContext.commit('setToken',res.token)
                })
                .catch(e=>console.log(e))
            }
        },
        getters:{
            loadedPosts(state){
                return state.loadedPosts
            }
        }
    })
}

export default createStore
